<?php

/**
 * @file
 * Specify common apps components to be created by apps_compatible.
 */

/**
 * Implements hook_apps_compatible_info().
 */
function paddle_google_analytics_apps_compatible_info() {
  return array(
    // Ensure a set of roles is created.
    'role' => array(
      'administrator' => array(
        'machine name' => 'administrator',
      ),
      'Chief Editor' => array(
        'machine name' => 'chief_editor',
      ),
      'Site Manager' => array(
        'machine name' => 'site_manager',
      ),
      'Editor' => array(
        'machine name' => 'editor',
      ),
    ),
  );
}
